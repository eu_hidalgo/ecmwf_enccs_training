{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/one_logo.png\" width=\"500\"/> \n",
    "\n",
    "# Interpolation from IFS Model levels to list of height levels in meters\n",
    "## HiDALGO/ENCCS training, 27 April 2021"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ECMWF IFS output data is available in set of vertical levels important for Meteorologists, such as pressure levels, potential temperature, potential vorticity as well as original Model Levels.  \n",
    "\n",
    "Some downstream applications and simulations, however, need the data on geometric heights in meters.  \n",
    "\n",
    "This notebook will help you interpolate the data on Model Levels into list of heights.  \n",
    "\n",
    "Metview has a function that handles this work called **ml_to_hl()**. This function is based on the Python eccodes script that does the interpolation. Documentation about this script and the code itself can be found on [Confluence](https://confluence.ecmwf.int/display/ECC/compute_wind_speed_height.py).    \n",
    "\n",
    "List of metview grib fieldset functions: on [Confluence](https://confluence.ecmwf.int/display/METV/Fieldset+Functions)  \n",
    "How to use these functions in Python: also on [Confluence documentation](https://confluence.ecmwf.int/display/METV/Using+Metview%27s+Python+Interface)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "!conda install --yes --prefix {sys.prefix} metview-batch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!{sys.executable} -m pip install metview"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import metview as mv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to interpolate from model levels to heights in meters, several other paramters need to be retrieved, beside the parameters that we're interpolating (u and v in this example).  \n",
    "\n",
    "These are:\n",
    "- temperature (t) and specific humidity (q) on all 137 levels (in files tq*.grib)\n",
    "- logarithm of mean sea level pressure **only on level 1**. This parameter is only available on level 1 which represent lowest level (in files lnsp*.grib). Normally the lowest Model level is 137, but in this special case level 1 represent the lowest and only available Model level  \n",
    "- geopotential is also available only on model level 1. However this surface geopotential is only a function of the model grid, the model orography and the model level definitions, and not of atmospheric structure or evolution, so it is not archived in forecast fields, but only as analysis, so we have only one field per model run"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we load the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tq = mv.read('data/UAP/tq3.grib')\n",
    "zlnsp = mv.read('data/UAP/zlnsp3.grib')\n",
    "uvt  = mv.read('data/UAP/uv3.grib')   #The data we want to interpolate\n",
    "z    = mv.read('data/UAP/z.grib')   #there is only one z file for one day."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we need to filter the parameters, since t and q are in the same file (as well as lnsp and z). We are using the same mv.read() function to filter grib data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t_one = mv.read(data=tq,param='t')\n",
    "q_one = mv.read(data=tq,param='q')\n",
    "\n",
    "lnsp_one = mv.read(data=zlnsp,param='lnsp')\n",
    "zs_one   = mv.read(data=z,param='z')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = q_one.to_dataset()\n",
    "ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to provide the list of parameters that will be interpolated. Those are stored in the file uv3.grib.   \n",
    "\n",
    "We can read from our grib file which parameters for interpolation are inside using grib_get_string Metview function.  \n",
    "You can read more on Metview grib functions in the [documentation](https://confluence.ecmwf.int/display/METV/Fieldset+Functions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(set(mv.grib_get_string (uvt, \"shortName\")))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters = list(set(mv.grib_get_string (uvt, \"shortName\")))\n",
    "parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need geopotential (z) on model levels, but that is not available for download, so we need to calculate it. Metview has built in function for this [mvl_geopotential_on_ml](https://confluence.ecmwf.int/display/METV/mvl_geopotential_on_ml).  \n",
    "This function uses temperature, specific humidity, logarithm of mean se level pressure and geopotential from level 1.  \n",
    "It returnes set of geopoint height fields on all 137 model levels. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z_one = mv.mvl_geopotential_on_ml(t_one, q_one, lnsp_one, zs_one)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will need the list of heights into which we want to interpolate our fields:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hlevs = [10,15, 20, 30, 50, 60, 70, 80, 90, 110, 150, 250, 300, 500, 600, 750, 900, 1000, 1250, 1500]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we need to initiate the empty grib field, to which we then append later grib fields."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "convert_all = mv.Fieldset()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have everything to calculate fields on geometric height.  \n",
    "Note that **ml_to_hl** only works with one parameter and one time step at the time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for parameter in parameters:\n",
    "    param_one = mv.read(data = uvt, param = parameter)\n",
    "    convert_param = mv.ml_to_hl(param_one, z_one, zs_one, hlevs, \"ground\", \"linear\")\n",
    "    convert_all.append(convert_param)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "file_name = 'output/metview_py_test_.grib'\n",
    "\n",
    "mv.write(file_name,convert_all) "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
