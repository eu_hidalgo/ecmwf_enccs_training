{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/one_logo.png\" width=\"500\"/> \n",
    "\n",
    "# Daily total precipitation for South Sudan from ERA5-Land dataset\n",
    "\n",
    "## HiDALGO/ENCCS training, 27 April 2021"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are getting total precipitation data from ERA5-Land dataset. ERA5-Land is a replay of the land component of the [ERA5 climate reanalysis](https://confluence.ecmwf.int/display/CKB/ERA5), but with a series of improvements making it more accurate for all types of land applications. In particular, ERA5-Land runs at enhanced resolution (9 km vs 31 km in ERA5). The temporal frequency of the output is hourly and the fields are masked for all oceans, making them lighter to handle.  \n",
    "\n",
    "Full ERA5-Land documentation can be found [here](https://confluence.ecmwf.int/display/CKB/ERA5-Land%3A+data+documentation).\n",
    "\n",
    "More about total precipitation parameter can be found [here](https://apps.ecmwf.int/codes/grib/param-db?id=228).  \n",
    "And the list of all available parameters in ERA5-Land dataset can be found on [this link](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land?tab=overview).  \n",
    "\n",
    "The **critical difference** between total precipitation data from ERA5 dataset and ERA5-Land dataset is the accumulation period. While in ERA5 dataset, the data from one timestamp contains data from previous to that hour, in ERA5-Land, data with timestamp for particular hour will have accumulated precipitation **since 00UTC up until that hour.** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import xarray as xr\n",
    "import cdsapi\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## CDS API request\n",
    "### Run the request\n",
    "\n",
    "Here is CDSAPI request for total precipitation from ERA5 Land dataset.  \n",
    "\n",
    "It will download **24 hours accumulated total precipitation (from 00 UTC one day to 00 UTC next day)** for the area of South Sudan. Please check if the area is big enough and change bounding box if needed. When downloaded, it will be around 25Mb file.  \n",
    "\n",
    "It will download the data for the whole 2016 and 2017, because we need some data from 2016 and some from 2017, so we need to put all the months in the request, resulting with getting both full years.  \n",
    "\n",
    "If you already have the data downloaded, don't run this cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = cdsapi.Client(url='https://cds.climate.copernicus.eu/api/v2',\n",
    "key='id:key')\n",
    "    \n",
    "c.retrieve(\n",
    "    'reanalysis-era5-land',\n",
    "    {\n",
    "        'variable':'total_precipitation',\n",
    "        'year':[\n",
    "            '2016','2017'\n",
    "        ],\n",
    "        'month':[\n",
    "            '01','02','03',\n",
    "            '04','05','06',\n",
    "            '07','08','09',\n",
    "            '10','11','12'\n",
    "        ],\n",
    "        'day':[\n",
    "            '01','02','03',\n",
    "            '04','05','06',\n",
    "            '07','08','09',\n",
    "            '10','11','12',\n",
    "            '13','14','15',\n",
    "            '16','17','18',\n",
    "            '19','20','21',\n",
    "            '22','23','24',\n",
    "            '25','26','27',\n",
    "            '28','29','30',\n",
    "            '31'\n",
    "        ],\n",
    "        'time':'00:00',\n",
    "        'format':'netcdf',\n",
    "        'area':['2','22','13','37'] #S,W,N,E\n",
    "    },\n",
    "    'data/tp_south_sudan.nc')  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load the data\n",
    "Open the dataset using xarray, and inspect the 'tp' variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_prec = xr.open_dataset('data/tp_south_sudan.nc')\n",
    "ds_prec.tp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_prec.tp.sel(latitude=10.1,longitude=30.2).to_dataframe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the ERA5 Land dataset each time step contains accumulated precipitation from 00 that day until that hour. All the times are in UTC.  \n",
    "\n",
    "01 hour has accumulated precipitation from 00 to 1UTC, 02 is from 0 to 2 UTC etc. 23 hour has the accumulated precipitation from 00 to 23 and finally, 00 has the whole previous 24 hours accumulated precipitation. This means that we have already downloaded daily precipitation and don't need to do any resampling.    \n",
    "\n",
    "**However, since 00 time of one day has the information of the previous day, this means the date in our time dimension is wrong. 2016-06-02 actually contains the data from 1 June 2016. To correct this we can shift the time dimension and then slice the interval we are interested in.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Precipitation are in meters here, that is why the numbers are so small, and we are used to them in milimeters, hence the conversion x1000."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tp_correct_time = ds_prec.shift(time=-1).sel(time=slice('2016-06-01', '2017-10-31')) * 1000\n",
    "tp_correct_time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "locations = pd.read_csv('data/MIG/locations.csv')\n",
    "lats = locations['lat']\n",
    "lons = locations['lon']\n",
    "lats_da = xr.DataArray(lats, dims=[\"z\"])\n",
    "lons_da = xr.DataArray(lons, dims=[\"z\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "locations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filtered_tp = tp_correct_time.sel(latitude=lats_da,longitude=lons_da,method = 'nearest')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Convert to pandas dataframe or save to netcdf\n",
    "Depending on how we want to save the output we can convert it to pandas dataframe or just save as netCDF."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "daily_tp_filtered_df = filtered_tp.to_dataframe()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "daily_tp_filtered_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "daily_tp_filtered_df.sort_values(by=['latitude','longitude'], inplace=True, ascending=False)\n",
    "daily_tp_filtered_df = daily_tp_filtered_df.reset_index()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "daily_tp_filtered_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "daily_tp_filtered_df = daily_tp_filtered_df[[\"latitude\",\"longitude\",\"time\",\"tp\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "daily_tp_filtered_df.to_csv('data/MIG/era5_land_tp_filtered.csv', index=False, sep='\\t',float_format = '%.1f')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
