# HiDALGO/ENCCS Weather and Climate data tutorial

<img src="images/one_logo.png" width=700/>

This repository hosts the Jupyter notebooks developed for the HiDALGO/ENCCS Weather and Climate Data tutorial 27th April 2021

## Tutorial outline  
Try it out on Binder:  
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/eu_hidalgo%2Fecmwf_enccs_training/master)  

This tutorial has the following outline:  

Downloading data:  
* [00 - Retrieve ERA5 Reanalysis - 2m temperature and 10m wind data example](./CDSAPI_example_00.ipynb)
* [01 - Retrieve ERA5 Reanalysis - Monthly data example](./CDSAPI_example_01.ipynb)
* [02 - Retrieve ERA5 Reanalysis - Ensemble data example ](./CDSAPI_example_02.ipynb)
* [03 - ECMWF seasonal forecast ](./CDSAPI_example_03.ipynb)
* [04 - ERA5 precipitation data ](./CDSAPI_example_04.ipynb)  
Processing data:  
* [01 - Exploring meteorological data](./Processing_meteorological_data_01.ipynb)
* [02 - Temporal resampling ](./Processing_meteorological_data_02.ipynb)
* [03 - Calculating rolling statistics ](./Processing_meteorological_data_03.ipynb)
* [04 - Working with ensemble data ](./Processing_meteorological_data_04.ipynb)
* [05 - Using Dask to chunk and open mulitiple files](./Processing_meteorological_data_05.ipynb)  
HiDALGO examples  
* [South Sudan daily precipitation from ERA5-Land ](./HiDALGO_Daily_total_precipitation_era5_land_South_Sudan.ipynb)
* [South Sudan GloFAS data example](./HiDALGO_GloFAS_example.ipynb)
* [Vertical interpolation example](./HiDALGO_vertical_interpolation.ipynb)
